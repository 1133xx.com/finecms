<?php
if (!defined('IN_FINECMS')) exit('No permission resources');

return array(
	'key' => 1,
    'name'    => '广告管理',
    'author'  => 'dayrui',
    'version' => '1.0',
    'typeid'  => 1,
    'description' => '广告管理系统插件'
);